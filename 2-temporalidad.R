setwd("~/GSOC-Argentina")

library(raster)

dat <- read.csv("ARGregMatrix_allcovs.csv")


dat <- dat[complete.cases(dat),]
dat$Date <- as.character(dat$Date)
# dat[dat$Date=='1190-01-01',3] <- '1990-01-01'
# dat[dat$Date=='1595-10-09',3] <- '1995-10-09'
# dat[dat$Date=='979-10-09',3] <- '1979-10-09'
dat$Date <- strptime(dat$Date, format='%Y-%m-%d')

summary(dat$Date)

dat$Date[dat$Date > "2018-01-01"] <- "2017-02-22"

years <- as.POSIXlt(dat$Date)$year + 1900
hist(years,  xlab="Year", breaks =63,  main="", col= "brown", freq=TRUE, las = 2, cex.axis=0.7, lab=c(1,5,7))

# library(ggplot2)
# ggplot(data = dat@data, aes(x =type, fill = Date)) + 
#   geom_bar()

################################################################################


factors <- shapefile("IPCC+Gran_Base_de_Datos/IPCC_gk5.shp")
factors <- spTransform(factors, CRS('+init=epsg:4326'))
factors@data <- factors@data[,c(1:6, 8)]

names(factors)[3:7] <- c('FMG5587', 'FMG8817', 'FI', 'FLU8817', 'FLU5587')

desmonte <- shapefile("SHP/1976-2012_P94F4_Argentina.shp")
desmonte@data[,1:2] <- NULL
desmonte <- spTransform(desmonte, CRS('+init=epsg:4326'))


############# point data ####
dat_sp <- dat
coordinates(dat_sp) <- ~ X + Y
dat_sp@proj4string <- factors@proj4string


years <- as.POSIXlt(dat$Date)$year + 1900

factors <- over(dat_sp, factors)
factors$DESMONTE <- over(dat_sp, desmonte)


############ Expand IPCC factor matrix #####
IPCCfactors <- list()
IPCCfactors$FI <- matrix(ncol=63, nrow=nrow(factors),
                         data=c(rep((1-factors$FI)/20, 33), 
                                rep((1-factors$FI)/20, 30)))  ### distribute the factors between the years
colnames(IPCCfactors$FI ) <- 1955:2017

IPCCfactors$FMG <- matrix(ncol=63, nrow=nrow(factors),
                   data=c(rep((1-factors$FMG5587)/20, 47),   ### acordamos considerar SD desde el 2001/2002
                          rep((1-factors$FMG8817)/20, 16)))  ### distribute the factors between the years
colnames(IPCCfactors$FMG ) <- 1955:2017

IPCCfactors$FLU <- matrix(ncol=63, nrow=nrow(factors),
                          data=c(rep((1-factors$FLU5587)/20, 33), 
                                 rep((1-factors$FLU8817)/20, 30)))  ### distribute the factors between the years
colnames(IPCCfactors$FLU ) <- 1955:2017

### Add desmonte to FLU
for(i in 1:nrow(factors)){
  if(!is.na(factors$DESMONTE[i,])){
    IPCCfactors$FLU[i,] <- 0
    top <- ifelse(factors$DESMONTE[i,]-1955+1+20 >63,63, factors$DESMONTE[i,]-1955+1+20)
    IPCCfactors$FLU[i,(factors$DESMONTE[i,]-1955+1) : top] <- 0.0155 ### (1-0.69)/20
  }
}


### final factor
IPCCfactors$TOTAL <- IPCCfactors$FI * IPCCfactors$FMG * IPCCfactors$FLU
colnames(IPCCfactors$TOTAL ) <- 1955:2017

#dat2 <- cbind(as.character(dat$Date), dat$Ftot_8817, dat$Ftot_6087, dat$OCSKGM, result)

temporal <- function(dat, year, factor){
  result <- vector()
  for(i in 1:nrow(dat)){
    ff <-factor[i, (as.character(year : years[i]))]
    if(years[i] == year){result[i] <- dat$OCSKGM[i]}
    if(years[i] > year){result[i] <- dat$OCSKGM[i] + (dat$OCSKGM[i] * sum(ff))}
    if(years[i] < year){result[i] <- dat$OCSKGM[i] - (dat$OCSKGM[i] * sum(ff))}
  }
  return(result)
}

dat$OCSKGM2001 <- temporal(dat, 2001, IPCCfactors$TOTAL)
dat$OCSKGM2015 <- temporal(dat, 2015, IPCCfactors$TOTAL)
# dat$diff20152001 <- dat$OCSKGM2015 - dat$OCSKGM2001
# dat$diff20152001 <- round(dat$diff20152001, 6)

write.csv(dat, "ARGregMatrix_IPCC_allcovs.csv")

