# Random forest qrf SOCARG
setwd("~/GSOC-Argentina")

#################################### 2001 ####


library(caret)
# Load data
dat <- read.csv("ARGregMatrix_IPCC_allcovs.csv")
dat$OCSKGM <- dat$OCSKGM2015

dat$Date <- strptime(dat$Date, format =  "%Y-%m-%d")
dat <- dat[dat$Date < "2014-01-01",]

datv <- read.csv("validation/val_141516.csv")
datv <- datv[complete.cases(datv),]
coordinates(datv) <- ~ X + Y


library(sp)

# Promote to spatialPointsDataFrame
coordinates(dat) <- ~ X + Y

library(maps)

plot(dat, pch = 20, cex=0.3, col="blue")
map("world", add=TRUE)

plot(dat, cex=0)
points(datv, pch = 20, cex=0.3, col="red", ext=dat)
map("world", add=TRUE)


## [1] "SpatialPointsDataFrame"
## attr(,"package")
## [1] "sp"

dat@proj4string <- CRS(projargs = "+init=epsg:4326")

dat@proj4string

## CRS arguments:
##  +init=epsg:4326 +proj=longlat +datum=WGS84 +no_defs
## +ellps=WGS84 +towgs84=0,0,0

library(raster)

# covs <- stack("stack1000.tif")
# 
# names(covs) <- readRDS("namesStack1000.rds")

# covs <- stack("stack1000.tif")
# names(covs) <- readRDS("namesStack1000.rds")
# covs <- stack("stackWorldgrids.tif")

 files <- list.files(path = "Layers/", pattern = "tif$", full.names = T)
 covs <- stack(files)

# For its use on R we need to define a model formula

dat@data <- dat@data[c("OCSKGM", names(covs))]

dat <- dat[complete.cases(dat@data),]


## Se trabaja solo con el 75% de los datos y se utilizan 25% para validar

inTrain <- createDataPartition(y = dat@data$OCSKGM, p = .80, list = FALSE)
training <- dat@data[ inTrain,]
testing <- dat@data[-inTrain,]

saveRDS(testing, "validation_Dataset2001.rds")

fm = as.formula(paste("OCSKGM ~", paste0(names(covs),
                                              collapse = "+")))

library(randomForest)

## randomForest 4.6-14
## Type rfNews() to see new features/changes/bug fixes.


## Loading required package: lattice
## Loading required package: ggplot2
## 
## Attaching package: 'ggplot2'
## The following object is masked from 'package:randomForest':
## 
##     margin
# Search for the best mtry parameter


#### Calibrar el modelo y preparar los datos de validacion
library(doParallel)
cl <- makeCluster(detectCores(), type='PSOCK')
registerDoParallel(cl)
control2 <- rfeControl(functions=rfFuncs, method="repeatedcv", number=10, repeats=10)

names(dat)
(rfmodel <- rfe(x=training[,2:35], y=training[,1], sizes=c(1:5), rfeControl=control2))

testing$residuals <- testing$OCSKGM - predict(rfmodel, testing)
stopCluster(cl = cl)

## Mapa de valos mas probable
beginCluster()

library(randomForest)


(predRFE <- clusterR(covs, predict, args=list(model=rfmodel)))



writeRaster(predRFE, file="OCSKGM2015.tif", 
            overwrite=TRUE)

plot(predRFE, col=colorRampPalette(c("gray", "brown", "blue"))(255), zlim=c(0,30))

endCluster()

rfmodel
# 
# Recursive feature selection
# 
# Outer resampling method: Cross-Validated (10 fold, repeated 10 times) 
# 
# Resampling performance over subset size:
#   
#   Variables  RMSE Rsquared   MAE RMSESD RsquaredSD   MAESD Selected
# 1 3.215   0.1588 2.189 0.3132    0.06979 0.11764         
# 2 2.955   0.2839 1.986 0.2726    0.07113 0.09661         
# 3 2.766   0.3702 1.820 0.2796    0.07298 0.09987         
# 4 2.704   0.3981 1.754 0.2865    0.07096 0.10445         
# 5 2.668   0.4144 1.718 0.2778    0.06480 0.08833         
# 34 2.643   0.4270 1.678 0.2800    0.06400 0.08548        *
#   
#   The top 5 variables (out of 34):
#   ARGPrecip_MayJunJul, DecJan_MeanTempArg_1km, ARGPrecip_AugSepOct, ARGPrecip_FebMarApr, ARG_chnl_dist






#################################################unc
#############################
# Mapa de incertidumbre #####

library(quantregForest)

model <- quantregForest(y=testing$residuals, x=testing[,2:64], ntree=500, keep.inbag=TRUE)

beginCluster(6,type="SOCK")

#Estimate model uncertainty

unc <- clusterR(covs[[names(testing[,2:31])]], predict, args=list(model=model,what=sd))

#salvar el mapa de incertidumbre

plot(unc, col=colorRampPalette(c("gray", "brown", "blue"))(255))

endCluster()

unc <- mask(unc, predRFE)


### Analisis de sensbilidadlibrary(Metrics)

library(Metrics)

#Generate an empty dataframe
library(caret)
validation <- data.frame(rmse=numeric(), r2=numeric())

pred <- predRFE
ctrl <- trainControl(method = "cv", savePred=T)

#Sensitivity to the dataset

#Start a loop with 10 model realizations

unregister <- function() {
  env <- foreach:::.foreachGlobals
  rm(list=ls(name=env), pos=env)
}

unregister()

for (i in 1:10){
  # We will build 10 models using random samples of 25%
  smp_size <- floor(0.25 * nrow(dat))
  train_ind <- sample(seq_len(nrow(dat)), size = smp_size)
  train <- dat[train_ind, ]
  test <- dat[-train_ind, ]
  modn <- caret::train(fm, data=train@data, method = "rf", trControl = ctrl)
  
  pred <- stack(pred, predict(covs, modn))
  test$pred <- extract(pred[[i+1]], test)
  # Store the results in a dataframe
}

#The sensitivity map is the dispersion of all individual models

sensitivity <- calc(pred[[-1]], sd)

sensitivity <- mask(sensitivity, predRFE)

writeRaster(sensitivity, file="OCSKGM_IPCC_qrf_jul26_sensibilidad.tif", 
            overwrite=TRUE)

# The total uncertainty is the sum of sensitivity and model

# uncertainty

unc2 <- unc + sensitivity

plot(unc2, col=colorRampPalette(c("gray", "brown", "blue"))(255))

writeRaster(unc2, file="OCSKGM_IPCC_qrf_jul26_incertidumbre.tif", 
           overwrite=TRUE)
